/** 
Proyecto: Juego de la vida.
 * Pruebas del almacén de datos.
 * @since: prototipo 0.1.2
 * @source: DatosTest.java 
 * @version: 0.1.2 - 2020/03/07
 * @author: ajp
 */

package accesoDatos;

import modelo.ClaveAcceso;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.ModeloException;
import modelo.Nif;
import modelo.Usuario;
import util.Fecha;
import util.Formato;

public class DatosTest {

	private static Datos almacen;
	
	public DatosTest() {
		almacen = Datos.getAlmacen();
	}
	
	/**
	 * Genera datos de prueba válidos dentro 
	 * del almacén de datos.
	 */
	public void cargarUsuariosPrueba() {
		String letrasNif = Formato.LETRAS_NIF; 	

		boolean condicion = false;
		do {
			try {
				for (int i = 3; i < 7; i++) {
					Usuario usuario = new Usuario(new Nif("0000000" + i + letrasNif.charAt(i)), 
							"Pepe", "López Pérez",
							new DireccionPostal("Iglesia", "12", "30012", "Patiño"),
							new Correo("pepe" + i + "@gmail.com"), new Fecha(1998, 3, 21), new Fecha(2019, 11, 17),
							new ClaveAcceso("Miau#" + i), Usuario.RolUsuario.NORMAL);
					almacen.altaUsuario(usuario);
				}
			} 
			catch (ModeloException e) {
				System.out.println("Error: datos incorrectos." + e.getMessage());
				condicion = true;
				//e.printStackTrace();
			} 
		} while (condicion);	
	}
	
	public static void main(String[] args) {
		DatosTest prueba = new DatosTest();
		prueba.cargarUsuariosPrueba();
		almacen.mostrarTodosUsuarios();

	}
}
