/** 
Proyecto: Juego de la vida.
 * Implementa el control de inicio de sesión y ejecución de la simulación por defecto. 	
 * @since: prototipo 0.1.0
 * @source: JVida.java 
 * @version: 0.2.0 - 2020/03/12
 * @author: ajp
 */

package _main;

import accesoDatos.Datos;
import accesoUsr.Presentacion;
import modelo.Simulacion;

public class JVida {

	private static Presentacion interfazUsr;
	private static Datos almacen;
	
	public JVida() {
		interfazUsr = new Presentacion();
		almacen = Datos.getAlmacen();
	}
	
	/**
	 * Secuencia principal del programa.
	 */
	public static void main(String[] args) {		
		
		new JVida();	
		almacen.mostrarTodosUsuarios();
		
		if (interfazUsr.inicioSesionCorrecto()) {
					
			System.out.println("Sesión: " + almacen.getNumSesionesRegistradas() + '\n' + "Iniciada por: " 
					+ interfazUsr.getSesion().getUsr().getNombre() + " " 
					+ interfazUsr.getSesion().getUsr().getApellidos());	
			
			Simulacion demo = almacen.consultarSimulacion("Demo0");
			interfazUsr.setSimulacion(demo);
			demo.getMundo().lanzarSimulacion(interfazUsr);
		}
		else {
			System.out.println("\nDemasiados intentos fallidos...");
		}		
		System.out.println("Fin del programa.");
	}

} 
