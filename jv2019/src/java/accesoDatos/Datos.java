/** 
Proyecto: Juego de la vida.
 * Implementa el almacén de datos.
 * @since: prototipo 0.1.1
 * @source: Datos.java 
 * @version: 0.1.2 - 2020/03/07
 * @author: ajp
 */

package accesoDatos;

import java.util.ArrayList;
import java.util.HashMap;

import modelo.ClaveAcceso;
import modelo.Correo;
import modelo.DireccionPostal;
import modelo.Identificable;
import modelo.ModeloException;
import modelo.Mundo;
import modelo.Nif;
import modelo.Sesion;
import modelo.Simulacion;
import modelo.Usuario;
import util.Fecha;
import util.Formato;

public class Datos {

	private static Datos almacen = new Datos();
	private ArrayList<Identificable> datosUsuarios;
	private ArrayList<Identificable> datosSesiones;
	private ArrayList<Identificable> datosSimulaciones;
	private ArrayList<Identificable> datosMundos;
	private HashMap<String,String> equivalenciasId;

	public Datos() {
		datosUsuarios = new ArrayList<Identificable>();
		datosSesiones = new ArrayList<Identificable>();
		datosSimulaciones = new ArrayList<Identificable>();
		datosMundos = new ArrayList<Identificable>();
		equivalenciasId = new HashMap<String,String>();
		this.iniciarAlmacen();
	}

	private void iniciarAlmacen() {
		cargarUsuariosIntegrados();
		cargarSimulacionDemo();
	}

	public static Datos getAlmacen() {
		return almacen;
	}

	/**
	 *  Obtiene por búsqueda binaria, la posición que ocupa, o ocuparía,  un objeto en la estructura.
	 *	@param Id - id del objeto a buscar.
	 * 	@param datos - Estructura de datos con elementos identificables (poseen un id utilizado para la busqueda y ordenación).
	 *	@return - la posición, en base 1, que ocupa un objeto o la que ocuparía (negativo).
	 */
	private int indexSort(ArrayList<Identificable> datos, String id) {
		int inicio = 0;
		int fin = datos.size() - 1;
		while (inicio <= fin) {
			int medio = (inicio+fin) / 2;	
			int comparacion = datos.get(medio).getId().compareTo(id);		
			if (comparacion == 0) {
				return medio+1; 	// Indices base 1
			}
			if (comparacion < 0) {
				inicio = medio+1;
			} 
			else {
				fin = medio-1;
			}
		}
		return -(fin+2); 			// Indices base 1
	}

	//USUARIOS
	///////////
	
	/**
	 * Obtiene un Usuario dado su identificador.
	 * @param id - el identificador del objeto a buscar.
	 * @return - el objeto encontrado o null si no existe.
	 */
	public Usuario consultarUsuario(String id) {	
		int index = indexSort(datosUsuarios, equivalenciasId.get(id));
		if (index < 0) {
			return null;
		}
		return (Usuario) datosUsuarios.get(index-1);
	}

	/**
	 * Nuevo usuario en el almacén.
	 * @param usr - el objeto a almacenar.
	 */
	public void altaUsuario(Usuario usr) {
		int index = indexSort(datosUsuarios, usr.getId());
		if (index < 0) {
			// index es en base 1
			datosUsuarios.add(-index-1, usr);
			altaEquivalId(usr);
		}
		else {
			int cont = 1;
			do {
				cont ++;
				usr.VariarId();
				index = indexSort(datosUsuarios, usr.getId());
				if (index < 0) {
					// index es en base 1
					datosUsuarios.add(-index-1, usr);
					altaEquivalId(usr);
					return;
				}
			}
			while (cont < Formato.LETRAS_NIF.length());
			throw new DatosException("Datos.altaUsuario: Id en uso...");
		}
	}

	private void altaEquivalId(Usuario usr) {
		equivalenciasId.put(usr.getId(), usr.getId());
		equivalenciasId.put(usr.getNif().getTexto(), usr.getId());
		equivalenciasId.put(usr.getCorreo().getTexto(), usr.getId());	
	}

	public Usuario bajaUsuario(String id) {
		int index = indexSort(datosUsuarios, id);
		if (index > 0) {
			Usuario usr = (Usuario) datosUsuarios.get(index-1);
			// index es en base 1
			datosUsuarios.remove(index-1);
			bajaEquivalId(usr);
			return usr;
		}
		throw new DatosException("Datos.bajaUsuario: el usuario no existe...");	
	}

	private void bajaEquivalId(Usuario usr) {
		equivalenciasId.remove(usr.getId());
		equivalenciasId.remove(usr.getNif().getTexto());
		equivalenciasId.remove(usr.getCorreo().getTexto());		
	}

	public Usuario modificarUsuario(Usuario usr) {
		int index = indexSort(datosUsuarios, usr.getId());
		if (index > 0) {
			Usuario aux = (Usuario) datosUsuarios.get(index-1);
			// index es en base 1
			datosUsuarios.set(index-1, usr);
			bajaEquivalId(aux);
			altaEquivalId(usr);
			return aux;
		}
		throw new DatosException("Datos.modificarUsuario: el usuario no existe...");
	}

	/**
	 * Muestra por consola todos los usuarios almacenados.
	 */
	public void mostrarTodosUsuarios() {
		for (Identificable u: datosUsuarios) {
			System.out.println("\n" + u);
		}
	}

	private void cargarUsuariosIntegrados() {

		Usuario admin = null;
		try {
			Usuario invitado = new Usuario();
			altaUsuario(invitado);

			admin = new Usuario(new Nif("00000001R"),
					"Admin", 
					"Admin Admin",
					new DireccionPostal(),
					new Correo("admin@correo.es"),
					new Fecha().addYears(-16),
					new Fecha(),
					new ClaveAcceso("Miau#0"),
					Usuario.RolUsuario.ADMIN
					);
			altaUsuario(admin);
		} 
		catch (ModeloException e) {
			e.printStackTrace();
		} 
		catch (DatosException e) {
			e.printStackTrace();
		}	
	}

	public int getUsuariosRegistrados() {
		return datosUsuarios.size();
	}

	//SESIONES
	///////////

	/**
	 * Obtiene una Sesion dado su identificador.
	 * @param id - el identificador del objeto a buscar.
	 * @return - el objeto encontrado o null si no existe.
	 */
	public Sesion consultarSesion(String id) {
		for (Identificable sesion : datosSesiones) {
			if (sesion != null && sesion.getId().equalsIgnoreCase(id)) {
				return (Sesion) sesion;	
			}
		}
		return null;						// No encuentra.
	}

	/**
	 * Nueva Sesion en el almacén.
	 * @param sesion - el objeto a almacenar.
	 */
	public void altaSesion(Sesion sesion) {
		int index = indexSort(datosSesiones, sesion.getId());
		if (index < 0) {
			// index es en base 1
			datosSesiones.add(-index-1, sesion);
			return;
		}
		throw new DatosException("Datos.altaSesion: el id está en uso...");	
	}

	public Sesion bajaSesion(String id) {
		int index = indexSort(datosSesiones, id);
		if (index > 0) {
			Sesion sesion = (Sesion) datosSesiones.get(index-1);
			// index es en base 1
			datosSesiones.remove(index-1);	
			return sesion;
		}
		throw new DatosException("Datos.bajaSesion: la sesión no existe...");
	}

	public Sesion modificarSesion(Sesion sesion) {
		int index = indexSort(datosSesiones, sesion.getId());
		if (index > 0) {
			Sesion aux = (Sesion) datosSimulaciones.get(index-1);
			// index es en base 1
			datosSesiones.set(index-1, sesion);
			return aux;
		}
		throw new DatosException("Datos.modificarSesion: la sesión no existe...");
	}

	public int getNumSesionesRegistradas() {
		return datosSesiones.size();
	}

	//SIMULACIONES
	//////////////

	/**
	 * Obtiene una Simulacion dado su identificador.
	 * @param id - el identificador del objeto a buscar.
	 * @return - el objeto encontrado o null si no existe.
	 */
	public Simulacion consultarSimulacion(String id) {
		if (id.equals("Demo0")) {
			return (Simulacion) datosSimulaciones.get(0);
		}
		for (Identificable simulacion : datosSimulaciones) {
			if (simulacion != null && simulacion.getId().equalsIgnoreCase(id)) {
				return (Simulacion) simulacion;	
			}
		}
		return null;						// No encuentra.
	}

	/**
	 * Registro de la simulación.
	 */
	public void altaSimulacion(Simulacion simulacion) {
		// Registra sesion, añade a partir de la última posición ocupada.
		datosSimulaciones.add(simulacion);  	
	}

	public Simulacion bajaSimulacion(String id) {
		int index = indexSort(datosSimulaciones, id);
		if (index > 0) {
			Simulacion simulacion = (Simulacion) datosSimulaciones.get(index-1);
			// index es en base 1
			datosSimulaciones.remove(index-1);
			return simulacion;
		}
		throw new DatosException("Datos.bajaSimulacion: la simulación no existe...");
	}

	public Simulacion modificarSimulacion(Simulacion simulacion) {
		int index = indexSort(datosSimulaciones, simulacion.getId());
		if (index > 0) {
			Simulacion aux = (Simulacion) datosSimulaciones.get(index-1);
			// index es en base 1
			datosSimulaciones.set(index-1, simulacion);
			return aux;
		}
		throw new DatosException("Datos.modificarSimulacion: la simulación no existe...");
	}

	public int getSimulacionesRegistradas() {
		return datosSimulaciones.size();
	}

	private void cargarSimulacionDemo() {
		try {
			Simulacion simulacionDemo = new Simulacion();
			altaSimulacion(simulacionDemo);
			Mundo mundoDemo = new Mundo();
			mundoDemo.setEspacio(cargarEspacioDemo());
			simulacionDemo.setMundo(mundoDemo);	
		} 
		catch (ModeloException e) {
			e.printStackTrace();
		}	
	}

	/**
	 * Carga datos demo en la matriz que representa el espacio del mundo. 
	 */
	private byte[][] cargarEspacioDemo() {
		// En este array los 0 indican celdas con células muertas y los 1 vivas.
		byte[][] espacioMundo = new byte[][] {  		
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 }, // 
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0 }, // 
			{ 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 1x Planeador
			{ 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 1x Flip-Flop
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, // 1x Still Life
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }, //
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }  //
		};
		return espacioMundo;
	}

	//MUNDOS
	////////
	
	/**
	 * Obtiene un Mundo dado su identificador.
	 * @param id - el identificador del objeto a buscar.
	 * @return - el objeto encontrado o null si no existe.
	 */
	public Mundo consultarMundo(String id) {
		for (Identificable mundo : datosMundos) {
			if (mundo != null && mundo.getId().equalsIgnoreCase(id)) {
				return (Mundo) mundo;	
			}
		}
		return null;						// No encuentra.
	}

	/**
	 * Nuevo objeto en el almacén.
	 * @param mundo - el objeto a almacenar.
	 */
	public void altaMundo(Sesion mundo) {
		int index = indexSort(datosMundos, mundo.getId());
		if (index < 0) {
			// index es en base 1
			datosSesiones.add(-index-1, mundo);
			return;
		}
		throw new DatosException("Datos.altaMundo: el id está en uso...");	
	}

	public Mundo bajaMundo(String id) {
		int index = indexSort(datosMundos, id);
		if (index > 0) {
			Mundo mundo = (Mundo) datosMundos.get(index-1);
			// index es en base 1
			datosMundos.remove(index-1);
			return mundo;
		}
		throw new DatosException("Datos.bajaMundo: el mundo no existe...");	
	}

	public Mundo modificarSesion(Mundo mundo) {
		int index = indexSort(datosMundos, mundo.getId());
		if (index > 0) {
			Mundo aux = (Mundo) datosMundos.get(index-1);
			// index es en base 1
			datosMundos.set(index-1, mundo);
			return aux;
		}
		throw new DatosException("Datos.modificarMundo: el mundo no existe...");
	}

	public int getNumMundosRegistrados() {
		return datosMundos.size();
	}

}
